import os     from 'os';
import detect from 'detect-port';

let net = os.networkInterfaces();
let currentIp = null;

Object.keys(net).forEach((ifname) => {
    let alias = 0;

    net[ifname].forEach((iface) => {
        if ('IPv4' !== iface.family || iface.internal !== false) return;
        currentIp = iface.address;
        ++alias;
    });
});

export default function () {
    let port = 3000;

    return {
        ip: currentIp,
        port: () => {
            detect(port)
                .then((_port) => {
                    let curr = null;

                    if (port === _port) {
                        console.log(`\n\n Port: ${ port } was not occupied.\n Work in ${currentIp}:${port}\n\n`);
                        curr = port;
                    } else {
                        console.log(`\n\n Port: ${ port } was occupied, connecting to port: ${ _port }`);
                        curr = _port;
                    }

                    return curr;
                })
                .catch(err => console.log(err));
        }
    };
}