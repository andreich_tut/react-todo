export default (path, autoprefixer) => {
    return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: [
                        require.resolve('style-loader'),
                        {
                            loader: require.resolve('css-loader'),
                            options: {
                                sourceMap: true,
                                modules: {
                                    localIdentName: "[local]_[hash:base64:5]"
                                },
                                importLoaders: 1,
                            }
                        },
                        {
                            loader: require.resolve('postcss-loader'),
                            options: {
                                ident: 'postcss',
                                plugins: () => [
                                    autoprefixer(),
                                ],
                            },
                        },
                        {
                            loader: require.resolve('sass-loader'),
                            options: {
                                includePaths: [path.styles],
                                data: '@import "./source/css/common/common";',
                            },
                        },
                    ]
                },
            ],
        },
    };
};