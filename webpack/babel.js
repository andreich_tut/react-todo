export default () => {
    return {
        module: {
            rules: [
                {
                    enforce: "pre",
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "eslint-loader"
                },
                {
                    test: /\.m?js$|\.jsx$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                }
            ]
        }
    }
};