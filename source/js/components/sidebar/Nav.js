import React     from 'react';
import PropTypes from 'prop-types';
import style     from 'css/components/sidebar/Nav.scss';

export default function Nav(props) {
    return (
        <nav className={ style.nav }>
            <button
                className={ style.navButton }
                onClick={ () => props.createCard() }>ADD TODO LIST
            </button>
            <button className={ style.navButton }>REMOVE ALL LISTS</button>
            <a className={ style.navButton } href="#">SEE SOURCE</a>
        </nav>
    );
}

Nav.propTypes = {
    createCard: PropTypes.func,
}