import React from 'react'
import style from 'css/components/sidebar/Logo.scss';

export default function Logo() {
    return (
        <h1 className={ style.logo }>
            Focking <span>a<span className={ style.w }>w</span><span className={ style.w }>w</span><span
            className={ style.w }>w</span>esome</span> todolist
        </h1>
    );
}