import React     from 'react';
import PropTypes from 'prop-types';
import style     from 'css/components/desk/TaskList.scss';
import Task      from './Task';

export default function TaskList(props) {
    return (
        <ul className={ style.list }>
            { props.list.map((task) => (
                <Task
                    key={ task.id }
                    task={ task }
                    removeMessage={ props.removeMessage }
                />
            )) }
        </ul>
    );
}

TaskList.propTypes = {
    list: PropTypes.array,
    removeMessage: PropTypes.func,
}