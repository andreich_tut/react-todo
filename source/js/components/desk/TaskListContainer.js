import React             from 'react';
import PropTypes         from 'prop-types';
import { connect }       from 'react-redux';
import { fetchTaskList } from 'js/store/taskList/actions';
import TaskList          from 'js/components/desk/TaskList';

// TODO: try using hooks here..

class CardListContainer extends React.Component {
    static propTypes = {
        cardId: PropTypes.number,
        list: PropTypes.array,
        fetchTaskList: PropTypes.func,
    }

    componentDidMount() {
        this.props.fetchTaskList(this.props.cardId);
    }

    render() {
        return (
            <TaskList list={ this.props.list }/>
        );
    }
}

const mapStateToProps = (state) => ({
    list: state.taskList.list,
});

const mapDispatchToProps = { fetchTaskList };

export default connect(mapStateToProps, mapDispatchToProps)(CardListContainer);