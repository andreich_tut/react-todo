import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import style                from 'css/components/desk/Task.scss';
import iconAcceptSvg        from 'static/images/icon-accept.svg';
import iconEditSvg          from 'static/images/icon-edit.svg';
import iconRemoveSvg        from 'static/images/icon-remove.svg';

// TODO: make it dumby

export default class Task extends Component {
    static propTypes = {
        task: PropTypes.shape({
            id: PropTypes.number,
            card_id: PropTypes.number,
            weight: PropTypes.number,
            text: PropTypes.string,
            checked: PropTypes.bool,
        }),
    };

    // constructor(props) {
    //     super(props);
    //     this.setCheckState = this.setCheckState.bind(this);
    //     this.removeMessage = this.removeMessage.bind(this);
    // }

    // setCheckState() {
    //     this.setState({ checked: !this.state.checked });
    // }

    // removeMessage(index) {
    //     this.props.removeMessage(index);
    // }

    render() {
        return (
            <li
                className={ this.props.task.checked ? style.taskChecked : style.task }
                key={ this.props.task.id }
            >
                <div className={ style.taskText }>
                    { this.props.task.weight + 1 }. { this.props.task.text }
                </div>
                <div className={ style.taskActions }>
                    <button className={ style.taskActionButton }>
                        <img src={ iconAcceptSvg } alt="icon-accept"/>
                    </button>

                    <button className={ style.taskActionButton }>
                        <img src={ iconEditSvg } alt="icon-edit"/>
                    </button>

                    <button className={ style.taskActionButton }>
                        <img src={ iconRemoveSvg } alt="icon-remove"/>
                    </button>
                </div>
            </li>
        );
    }
}