import React             from 'react';
import PropTypes         from 'prop-types';
import { connect }       from 'react-redux';
import { fetchCardList } from 'js/store/cardList/actions';
import CardList          from './CardList';

class CardListContainer extends React.Component {
    static propTypes = {
        list: PropTypes.array,
        fetchCardList: PropTypes.func,
    }

    componentDidMount() {
        this.props.fetchCardList();
    }

    render() {
        return (
            <CardList list={ this.props.list }/>
        );
    }
}

const mapStateToProps = (state) => ({
    list: state.cardList.list,
});

const mapDispatchToProps = { fetchCardList };

export default connect(mapStateToProps, mapDispatchToProps)(CardListContainer);