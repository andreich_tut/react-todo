import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import style                from 'css/components/desk/Card.scss';
import CardHeader           from './CardHeader';
import TaskListContainer    from './TaskListContainer';

// TODO: do something here..

export default class Card extends Component {
    static propTypes = {
        card: PropTypes.shape({
            id: PropTypes.number,
            title: PropTypes.string,
        }),
    };

    constructor(props) {
        super(props);

        this.removeMessage = this.removeMessage.bind(this);
        this.addMessageByKey = this.addMessageByKey.bind(this);
        this.addMessageByButton = this.addMessageByButton.bind(this);
    }

    addMessage(value) {
        const { taskList } = this.state;
        const message = { message: value };
        taskList.push(message);
        this.setState({ message, taskList });
    }

    addMessageByKey(e) {
        if (e.keyCode === 13) {
            const { value } = e.target;
            e.target.value = '';
            this.addMessage(value);
        }

        e.preventDefault();
        e.stopPropagation();
    }

    addMessageByButton(e) {
        const { value } = e.currentTarget.previousSibling;
        e.currentTarget.previousSibling.value = '';
        this.addMessage(value);
    }

    removeMessage(index) {
        const currentList = this.state.taskList;
        currentList.splice(index, 1);
        this.setState({ taskList: currentList });
    }

    render() {
        return (
            <article className={ style.card }>
                <CardHeader
                    title={ this.props.card.title }
                    addMessageByKey={ this.addMessageByKey }
                    addMessageByButton={ this.addMessageByButton }
                />
                <TaskListContainer
                    cardId={ this.props.card.id }
                />
            </article>
        );
    }
}