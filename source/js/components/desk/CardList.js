import React     from 'react';
import PropTypes from 'prop-types';
import style     from 'css/components/desk/CardList.scss';
import Card      from './Card';

export default function CardList(props) {
    return (
        <ul className={ style.cardList }>
            { props.list.map((card) => (
                <Card key={ card.id } card={ card }/>
            )) }
        </ul>
    );
}

CardList.propTypes = {
    list: PropTypes.array,
};