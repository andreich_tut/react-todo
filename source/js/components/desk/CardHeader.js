import React     from 'react';
import PropTypes from 'prop-types';
import style     from 'css/components/desk/CardHeader.scss';
import Textarea  from 'js/modules/TextArea';

export default function CardHeader(props) {
    return (
        <header className={ style.cardHeader }>
            <Textarea
                value={ props.title }
                placeholder="Type your title here..."
                outerClassName={ style.cardTitleArea }
                maxLength={ 50 }
            />
            <Textarea
                placeholder="Put your another awwwsome task here"
                outerClassName={ style.cardTaskArea }
                maxLength={ 130 }
                addMessageByKey={ props.addMessageByKey }
            />
            <button
                className={ style.cardSendButton }
                onClick={ e => props.addMessageByButton(e) }
            >
                pin it
            </button>
        </header>
    );
}

CardHeader.propTypes = {
    title: PropTypes.string,
    addMessageByKey: PropTypes.func,
    addMessageByButton: PropTypes.func,
}