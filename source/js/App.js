import React, { Component }             from 'react'
import { applyMiddleware, createStore } from 'redux';
import { Provider }                     from 'react-redux';
import thunk                            from 'redux-thunk';
import logger                           from 'redux-logger';
import style                            from 'css/App.scss';
import rootReducer                      from './store/reducers';
import Logo                             from './components/sidebar/Logo';
import Nav                              from './components/sidebar/Nav';
import Card                             from './components/desk/Card';
import CardListContainer                from './components/desk/CardListContainer';

const store = createStore(rootReducer, applyMiddleware(logger, thunk));

export default class App extends Component {
    constructor(props) {
        super(props);
        this.createCard = this.createCard.bind(this);
    }

    createCard() {
        this.state.cardList.push(<Card/>);
    }

    render() {
        return (
            <Provider store={ store }>
                <div className={ style.app }>
                    <aside className={ style.sidebar }>
                        <Logo/>
                        <Nav createCard={ this.createCard }/>
                    </aside>
                    <section className={ style.content }>
                        <CardListContainer/>
                    </section>
                </div>
            </Provider>
        );
    }
}