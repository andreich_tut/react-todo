import { SET_CARD_LIST } from 'js/store/cardList/actions';

const defaultState = {
    list: [],
}

export const cardListReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_CARD_LIST:
            return { ...state, list: action.payload };

        default:
            return state;
    }
}