import Axios from 'axios';

export const SET_CARD_LIST = 'SET_CARD_LIST';

export const setCardList = (list) => ({
    type: SET_CARD_LIST,
    payload: list,
});

export const fetchCardList = () => (dispatch) => {
    return Axios
        .get('https://my-json-server.typicode.com/andreich-tut/db-demo/cards')
        .then(({ data }) => {
            dispatch(setCardList(data));
        });
};