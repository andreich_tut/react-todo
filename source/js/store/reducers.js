import { combineReducers } from 'redux';
import { cardListReducer } from 'js/store/cardList/reducers';
import { taskListReducer } from 'js/store/taskList/reducers';

export default combineReducers({
    cardList: cardListReducer,
    taskList: taskListReducer,
})