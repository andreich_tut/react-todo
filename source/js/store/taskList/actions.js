import Axios from 'axios';

export const SET_TASK_LIST = 'SET_TASK_LIST';

export const setTaskList = (list) => ({
    type: SET_TASK_LIST,
    payload: list,
});

export const fetchTaskList = (cardId) => (dispatch) => {
    return Axios
        .get('https://my-json-server.typicode.com/andreich-tut/db-demo/tasks')
        .then(({ data }) => {
            dispatch(
                setTaskList(
                    data.filter(task => {
                        return task.card_id === cardId;
                    })
                )
            );
        });
}