import { SET_TASK_LIST } from 'js/store/taskList/actions';

const defaultState = {
    list: [],
}

export const taskListReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_TASK_LIST:
            return { ...state, list: action.payload };

        default:
            return state;
    }
}