import React, { Component } from 'react';
import PropTypes            from 'prop-types';

// TODO: states, hooooks???

class Textarea extends Component {
    static propTypes = {
        value: PropTypes.string,
        placeholder: PropTypes.string,
        outerClassName: PropTypes.string,
        maxLength: PropTypes.number,
        minRows: PropTypes.number,
        rows: PropTypes.number,
        addMessageByKey: PropTypes.func,
    };

    componentDidMount() {
        const area = document.querySelector(`.${ this.props.outerClassName }`);
        const lineHeight = parseFloat(getComputedStyle(area).lineHeight);
        const baseScrollHeight = area.scrollHeight;
        this.setState({ lineHeight, baseScrollHeight });
    }

    areaInputHandler(e) {
        const area = e.currentTarget;
        const { baseScrollHeight, lineHeight } = this.state;
        area.rows = this.props.minRows || 1;
        const rows = Math.ceil((area.scrollHeight - baseScrollHeight) / lineHeight);
        area.rows += rows;
    }

    areaKeyUpHandler(e) {
        return this.props.addMessageByKey ? this.props.addMessageByKey(e) : null;
    }

    render() {
        return (
            <textarea
                className={ this.props.outerClassName }
                defaultValue={ this.props.value || '' }
                placeholder={ this.props.placeholder }
                maxLength={ this.props.maxLength || 75 }
                rows={ this.props.rows || 1 }
                onInput={ e => this.areaInputHandler(e) }
                onKeyUp={ e => this.areaKeyUpHandler(e) }
            />
        );
    }
}

export default Textarea;