'use strict';

import path              from 'path';
import autoprefixer      from 'autoprefixer';
import ProgressBar       from 'progress-bar-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import merge             from 'webpack-merge';
import getAddress        from './webpack/address';
import babel             from './webpack/babel';
import css               from './webpack/css';
import files             from './webpack/files';

const PATHS = {
    build: path.join(__dirname, 'build'),
    public: path.join(__dirname, 'public'),
    source: path.join(__dirname, 'source'),
    static: path.join(__dirname, 'static'),
    js: path.join(__dirname, 'source/js'),
    css: path.join(__dirname, 'source/css'),
};

export default () => {
    const address = getAddress();
    const { ip } = address;
    const port = address.port();

    return merge([
        {
            mode: 'development',
            entry: `${ PATHS.public }/index.js`,
            output: {
                path: PATHS.build,
                filename: 'js/[name].js',
            },
            devtool: '#cheap-module-source-map',
            plugins: [
                new ProgressBar(),
                new HtmlWebpackPlugin({
                    filename: 'index.html',
                    template: `${ PATHS.public }/index.html`,
                    favicon: `${ PATHS.static }/favicon/favicon.ico`,
                }),
            ],
            resolve: {
                alias: {
                    css: path.resolve(PATHS.css),
                    js: path.resolve(PATHS.js),
                    static: path.resolve(PATHS.static),
                },
            },
        },
        babel(),
        files(),
        css(path, autoprefixer),
        {
            devServer: {
                host: ip,
                port: port,
                disableHostCheck: true,
                hot: true,
                noInfo: true,
                inline: true,
                contentBase: PATHS.source,
                watchOptions: {
                    ignored: /node_modules/,
                },
            },
        },
    ]);
};